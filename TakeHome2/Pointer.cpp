#include <stdio.h>
/**
* @brief	: This function calculates the sum and difference of two numbers with the help of pointers .
* @param	: Includes *a and *b parameters .
*/
void update(int* a, int* b) {
    int sum, difference;
    sum = *a + *b;
    if (*a > * b)
        difference = *a - *b;
    if (*b > * a)
        difference = *b - *a;
    *a = sum;
    *b = difference;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}
