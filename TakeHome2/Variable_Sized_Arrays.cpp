#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n, q;
    cin >> n >> q;
    /// Used vector to create variable size array .
    vector<int> arr[n];

    for (int i = 0; i < n; i++)
    {
        int size;
        cin >> size;
        int element;
        for (int j = 0; j < size; j++)
        {
            cin >> element;
            arr[i].push_back(element);
        }
    }
    int row, index;
    for (int i = 0; i < q; i++)
    {
        cin >> row >> index;
        cout << arr[row][index] << endl;
    }

    return 0;
}
