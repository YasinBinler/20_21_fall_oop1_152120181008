#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
/**
* @brief	: This function separates integers in a string from commas .
* @param	: Includes str parameter .
*/
vector<int> parseInts(string str) {
    vector<int> ints;
    stringstream ss(str);
    char c;
    int num;

    while (ss) {
        ss >> num >> c;
        ints.push_back(num);
    }
    return ints;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}
