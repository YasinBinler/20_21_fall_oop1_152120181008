#include <iostream>
#include <cstdio>
using namespace std;
/**
* @brief	: This function gives the highest parameters .
* @param	: Includes a , b , c and d parameters .
*/

int max_of_four(int a, int b, int c, int d)
{
    int highest = a;
    if (b > highest)highest = b;
    if (c > highest)highest = c;
    if (d > highest)highest = d;
    return highest;
}


int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}
