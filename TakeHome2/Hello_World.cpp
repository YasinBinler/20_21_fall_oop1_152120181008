/**
*@mainpage Object Oriented Programming I Lab Experiment 2
*   This is the main page for the project.
*
*1. In this project, there are c ++ projects solved by me through hackerrank .
*2. Information about the programmer can be found in the related pages section .
*
*/
/**
* @page       Information
* @author   : Yasin Binler - 152120181008
* @date     : 11 November 2020
*/
#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    printf("Hello, World!");
    return 0;
}