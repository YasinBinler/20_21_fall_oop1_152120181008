#include <iostream>
#include <sstream>
using namespace std;
/**
* @brief	: There are functions that set student information in this class.
* @brief	: There are functions in this class that bring student information.
* @brief	: There is a function in this class that converts student information to strings.
* @param	: Includes age , first name , last name and standard parameters .
*/
class Student {
private:
    int age, standart;
    string first_name, last_name;
public:
    void set_age(int a) {
        age = a;
    }
    void set_standard(int b) {
        standart = b;
    }
    void set_first_name(string c) {
        first_name = c;
    }
    void set_last_name(string d) {
        last_name = d;
    }
    int get_age() {
        return age;
    }
    string get_last_name() {
        return last_name;
    }
    string get_first_name() {
        return first_name;
    }
    int get_standard() {
        return standart;
    }
    string to_string()
    {
        stringstream ss;
        char c = ',';
        ss << age << c << first_name << c << last_name << c << standart;
        return ss.str();
    }
};
int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}
