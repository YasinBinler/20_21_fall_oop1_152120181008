/**
*@mainpage Object Oriented Programming Lab 01
*   This is the main page for the project.
*
*1. This program reads integers from the file and performs some operations on them .
*2. Information about the programmer can be found in the related pages section .
*
*/
/**
* @page       Information
* @author   : Yasin Binler - 152120181008
* @date     : 4 November 2020
*/
#include<iostream>
#include<fstream>
#include <iomanip>
using namespace std;
/**
* @brief	: This function gives the sum of all the elements in the array .
* @param	: Includes array and integer count parameters .
*/
int Sum(int* array, int num_of_number)
{
	int sum = 0;
	for (int i = 0; i < num_of_number; i++) {
		sum += array[i];
	}
	return sum;
}
/**
* @brief	: This function gives the product of all elements in the array .
* @param	: Includes array and integer count parameters .
*/
int Product(int* array, int num_of_number)
{
	unsigned long long int product = 1;
	for (int i = 0; i < num_of_number; i++) {
		product *= array[i];
	}
	return product;
}/**
* @brief	: This function gives the average of all elements in the array .
* @param	: Includes array and integer count parameters .
*/
float Average(int* array, int num_of_number)
{
	float average = 0, sum = 0;

	for (int i = 0; i < num_of_number; i++) {
		sum += array[i];
	}
	average = sum / num_of_number;
	return average;
}
/**
* @brief	: This function gives the smallest element in the array.
* @param	: Includes array and integer count parameters .
*/
int Smallest(int* array, int num_of_number)
{
	int small = array[0];
	for (int i = 1; i < num_of_number; i++) {
		if (array[i] < small)small = array[i];
	}

	return small;
}
/**
* @brief	: This is the main function of the program .
* @brief	: First, the file opens .
* @brief	: The file format is checked .
* @brief	: If the file format is wrong , an error message is given and the program off .
* @brief	: If the file format is correct functions are called .
*/
void main()
{
	ifstream dataFile;
	dataFile.open("input.txt");

	if (dataFile.is_open())
	{
		int num_of_number;
		dataFile >> num_of_number;
		// Code that returns an error if the First Data Type is not an integer .
		if (dataFile.fail())
		{
			cout << "Input is incorrect. Please check the input !!!" << endl;
			exit(0);
		}
		dataFile.close();

		dataFile.open("input.txt");
		int x,ctr=0;
		while (!dataFile.eof()) 
		{
			dataFile >> x;
			//Code that returns an error if the type of any data is not an integer .
			if (dataFile.fail())
			{
				cout << "Input is incorrect. Please check the input !!!" << endl;
				exit(0);
			}
			ctr++;
		}
		// Code that gives an error if the number of integers is more than or less than the specified amount.
		if (num_of_number + 1 != ctr)
		{
			cout << "Input is incorrect. Please check the input !!!" << endl;
			exit(0);
		}
		dataFile.close();

		dataFile.open("input.txt");

		dataFile.ignore(60, '\n');

		int y, ctr2 = 0;
		while (!dataFile.eof())
		{
			dataFile >> y;
			ctr2++;
		}
		// Code that gives an error if there are more than one integer in the first row .
		if (ctr2 != num_of_number)
		{
			cout << "Input is incorrect. Please check the input !!!" << endl;
			exit(0);
		}
		dataFile.close();

		dataFile.open("input.txt");
		dataFile >> num_of_number;

		int* numbers;
		numbers = new int[num_of_number];
		for (int i = 0; i < num_of_number; i++) {
			dataFile >> numbers[i];
		}
		cout << "Sum is " << Sum(numbers, num_of_number) << endl;
		cout << "Product is " << Product(numbers, num_of_number) << endl;
		cout << "Average is " << fixed << setprecision(2) << Average(numbers, num_of_number) << endl;
		cout << "Smallest is " << Smallest(numbers, num_of_number) << endl;
	}
	//Code that gives an error if the file does not exist .
	else cout << "The File Could Not Be Opened!!!" << endl;

	system("pause");
}